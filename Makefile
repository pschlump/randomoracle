all: 
	../bin/cmp-local.sh

setup:
	mkdir -p ./db

build_linux:
	../bin/cmp-prod.sh RandomOracle.linux

mac: all
	mv go-server go-server.mac

deploy_74: 
	../bin/cmp-prod.sh RandomOracle.linux
	echo ssh pschlump@192.154.97.74 "mkdir -p ./tools/RandomOracle/static"
	-ssh pschlump@192.154.97.74 "mv ./tools/RandomOracle/RandomOracle.linux ,aaaaa"
	scp *.linux pschlump@192.154.97.74:/home/pschlump/tools/RandomOracle
	scp *.sh pschlump@192.154.97.74:/home/pschlump/tools/RandomOracle
	rsync -r -a -v -e "ssh -l pschlump"    ./static            			pschlump@192.154.97.74:/home/pschlump/tools/RandomOracle
	echo "deploy-to-prod: " ${GIT_COMMIT} `date` >>build-log.txt 
.PHONY: deploy_74
