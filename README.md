# Random Oracle

This provides random data that changes every 2 minutes.

To Run

```bash

$ make
$ ./go-server 

```

You can change the port with -P port

You can change the directory with -D directory



# Get A Random Value

A call to `/RandomValue` will return JSON with the time to live and the random value.  If you supply a "callback" then the result will
be JSONp as a javascript file.  The call also supports CORS requests from any source.

```
http://agroledge.com:10000/RandomValue?callback=callback1121221
```

Exmaple Output:

```
{ "status":"success", "value":"1e6d0acea6dd50e31318d20f88b853c9855c3b883aed668216ac178a9e5091ae", "ttl":53, "ep":13024877 }
```

# Check Status

You can also check the status of the service with a call to:


```
http://agroledge.com:10000/RandomStatus
```

This will echo the request and show you the response headers.
The compiled version number for the RandomOracle is also listed.


