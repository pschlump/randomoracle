// Simple HTTP server

package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/American-Certified-Brands/tools/GetVar"
	flags "github.com/jessevdk/go-flags"
)

var GitCommit string

var Dir string
var Port string
var Duration int

var opts struct {
	Port     string `short:"p" long:"port" description:"A port number, ':Port', to listen on, or a host:port" default:":10000"`
	Dir      string `short:"d" long:"dir" description:"A directory to serve - default '.'" default:"./static"`
	Duration int    `short:"T" long:"time" description:"Duration in seconds for each random value to last." default:"120"`
	Version  bool   `short:"V" long:"version" description:"Return version info and exit."`
}

func ParseCmdLineArgs() {

	// args, err := flags.ParseArgs(&opts, os.Args)
	_, err := flags.ParseArgs(&opts, os.Args)

	if err != nil {
		panic(err)
		// os.Exit(1)
	}

	if opts.Version {
		fmt.Printf("Version (Git Commit): %s\n", GitCommit)
		os.Exit(0)
	}

	if opts.Dir == "." {
		Dir, _ = os.Getwd()
	} else {
		Dir = opts.Dir
	}

	Port = opts.Port
	Duration = opts.Duration
	ttlCurrent = opts.Duration

	setup0()
}

// -------------------------------------------------------------------------------------------------
func respHandlerStatus(www http.ResponseWriter, req *http.Request) {
	setupResponse(&www, req)
	if req.Method == "OPTIONS" {
		return
	}
	q := req.RequestURI

	var rv string
	www.Header().Set("Content-Type", "application/json")
	rv = fmt.Sprintf(`{"status":"success", "version":%q, "name":"go-server version 1.0.0", "URI":%q,"req":%s, "response_header":%s}`,
		GitCommit, q, SVarI(req), SVarI(www.Header()))

	io.WriteString(www, rv)
}

// -------------------------------------------------------------------------------------------------
var aValue []byte
var ttlCurrent int
var epoc_120 int64

func setup0() {
	var err error
	aValue, _ = GenRandBytes(32)
	ttlCurrent = Duration
	epoc_120 = (int64(time.Now().Unix()) / int64(Duration))
	ticker := time.NewTicker(time.Millisecond * 500)
	go func() {
		for t := range ticker.C {
			ttlCurrent--
			if db2 {
				fmt.Println("Tick at", t, ttlCurrent)
			}
			if ttlCurrent <= 0 {
				aValue, err = GenRandBytes(32)
				epoc_120 = (int64(time.Now().Unix()) / int64(Duration))
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error on random generation: %s\n", err)
					os.Exit(1)
				}
				ttlCurrent = Duration
			}
		}
	}()
	// time.Sleep(time.Millisecond * 1500)
	// ticker.Stop()
	// fmt.Println("Ticker stopped")
}

func setupResponse(www *http.ResponseWriter, req *http.Request) {
	(*www).Header().Set("Access-Control-Allow-Origin", "*")
	(*www).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*www).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func respHandlerRandomValue(www http.ResponseWriter, req *http.Request) {
	setupResponse(&www, req)
	if req.Method == "OPTIONS" {
		return
	}
	// func GetVar(name string, www http.ResponseWriter, req *http.Request) (found bool, value string) {
	found_jsonp, callback := GetVar.GetVar("callback", www, req)
	rv := fmt.Sprintf(`{ "status":"success", "value":"%x", "ttl":%d, "ep":%v }`, aValue, ttlCurrent, epoc_120)
	if found_jsonp {
		www.Header().Set("Content-Type", "application/javascript")
		rv = fmt.Sprintf("%s(%s);\n", callback, rv)
	} else {
		www.Header().Set("Content-Type", "application/json")
	}
	fmt.Fprintf(www, `%s`, rv)

}

// -------------------------------------------------------------------------------------------------
func main() {
	ParseCmdLineArgs()
	http.HandleFunc("/RandomStatus", respHandlerStatus)
	http.HandleFunc("/RandomValue", respHandlerRandomValue)
	http.Handle("/", http.FileServer(http.Dir(Dir)))
	// port := "8765"
	// dir := "/home/pschlump/www/sketchground/pjs/99-static/_site"
	// h := http.StripPrefix(pfx, http.FileServer(http.Dir("/home/spyros/src/ sandbox/board/pieces")))
	// http.Handle("/", http.FileServer(http.Dir(Dir)))
	// log.Fatal(http.ListenAndServe(":"+Port, http.FileServer(http.Dir(Dir))))
	log.Fatal(http.ListenAndServe(Port, nil))
}

const db2 = false
